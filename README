DUNE-library
============

DUNE, the Distributed and Unified Numerics Environment is a modular toolbox
for solving partial differential equations with grid-based methods.

The main intention is to create slim interfaces allowing an efficient use of
legacy and/or new libraries. Using C++ techniques DUNE allows to use very
different implementation of the same concept (i.e. grid, solver, ...) under
a common interface with a very low overhead.

DUNE was designed with flexibility in mind. It supports easy discretization
using methods, like Finite Elements, Finite Volume and also Finite
Differences. Through separation of data structures DUNE allows fast Linear
Algebra like provided in the ISTL module, or usage of external libraries
like blas.

This package contains the basic DUNE grid HOWTO classes.

License
-------

The DUNE-library and headers are licensed under version 2 of the GNU
General Public License, with the so-called "runtime exception", as
follows:

   As a special exception, you may use the DUNE source files as part
   of a software library or application without restriction.
   Specifically, if other files instantiate templates or use macros or
   inline functions from one or more of the DUNE source files, or you
   compile one or more of the DUNE source files and link them with
   other files to produce an executable, this does not by itself cause
   the resulting executable to be covered by the GNU General Public
   License.  This exception does not however invalidate any other
   reasons why the executable file might be covered by the GNU General
   Public License.

This licence clones the former one of the libstc++ library.

See the file COPYING for full copying permissions.

Installation
------------

Short installation instructions can be found in file INSTALL. For the
full instructions please see [0].

Links
-----

0. http://www.dune-project.org/doc/installation-notes.html
